md=readRDS("~/onedrive/rfiles/data/daily/ind/CNXDIVIDENDOPPORTUNITIES_IND___.rds")
# fix missing index data
files=list.files("~/onedrive/tempindex/",full.names = TRUE)
df=data.frame()
for (f in files){
  df=rbind(df,read.csv(f,stringsAsFactors = FALSE))
}
names(df)=c("date","open","high","low","close","volume","tradedvalue")
df$settle=df$close
df$date=as.POSIXct(df$date,format="%d-%b-%Y")
df$open=ifelse(!is.na(as.numeric(df$open)),as.numeric(df$open),0)
df$high=ifelse(!is.na(as.numeric(df$high)),as.numeric(df$high),0)
df$low=ifelse(!is.na(as.numeric(df$low)),as.numeric(df$low),0)
df$volume=ifelse(!is.na(as.numeric(df$volume)),as.numeric(df$volume),0)
df$tradedvalue=ifelse(!is.na(as.numeric(df$tradedvalue)),as.numeric(df$tradedvalue)*10,0)

# missing rows in md
missingrows=which(is.na(match(df$date,md$date)))
df=df[missingrows,]
md=dplyr::bind_rows(md,df)
md$symbol="CNXDIVIDENDOPPORTUNITIES_IND___"
md=md[order(md$date),]
rownames(md)=NULL
saveRDS(md,"~/onedrive/rfiles/data/daily/ind/CNXDIVIDENDOPPORTUNITIES_IND___.rds")


# fix missing pe
files=list.files("~/onedrive/temp/",full.names = TRUE)
df=data.frame()
for (f in files){
  df=rbind(df,read.csv(f,stringsAsFactors = FALSE))
}
names(df)=c("date","pe","pb","dividendyield")
df$date=as.POSIXct(df$date,format="%d-%b-%Y")
indicesToReplace=match(df$date,md$date)
if(length(which(is.na(indicesToReplace)))>0){
  dates=df[which(is.na(indicesToReplace)),c("date")]
  print(unique(strftime(dates,"%Y")))
  df=df[-which(is.na(indicesToReplace)),]
  indicesToReplace=na.exclude(indicesToReplace)
}
md[indicesToReplace,c("pe","pb","dividendyield")]=df[,c("pe","pb","dividendyield")]
rownames(md)=NULL
saveRDS(md,"~/onedrive/rfiles/data/daily/ind/CNXDIVIDENDOPPORTUNITIES_IND___.rds")


tail(which(md$pe==0|(is.na(md$pe))),20)
md[c(4783,4821,4827,4953),]

files=list.files("~/onedrive/rfiles/data/daily/ind/",full.names = TRUE)
indexnames=character()
for(f in files){
  md=readRDS(f)
  if(md$date[1]<"2012-03-01"){
    indexnames=c(indexnames,f)
  }
}
