dyn.load("/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/server/libjvm.so")
library(redux)
library(htmltab)
library(dplyr)
library(RTrade)
setwd("/home/psharma/onedrive/rfiles/data/static/")
proper=function(s) sub("(.)", ("\\U\\1"), tolower(s), pe=TRUE)
processingdate=Sys.time()
processingdate=strftime(processingdate,format="%Y%m%d",tz="Asia/Kolkata")
args.commandline = commandArgs(trailingOnly = TRUE)
if (length(args.commandline) > 0) {
        args <- args.commandline
}
processingdate=args[1]
#### update all symbols ####
urlBase="https://www.interactivebrokers.com/en/index.php?f=2222&exch=nse&showcategories=STK&showproducts="
constant = "&page="
out=data.frame()
for (pageno in seq(1,100) ){
        url= paste(urlBase,constant,pageno,sep="")
        out.tmp=htmltab(url,which=3)
        if(nrow(out.tmp)>0){
                out=rbind(out,out.tmp)
        }else{
                break
        }
        print(url)
}

names(out)=c("ibsymbol","longname","exchangesymbol","currency")
out$exchangesymbol=sapply(strsplit(out$exchangesymbol,"_"),"[",1)

# remove known duplicates/errors
exclude=numeric()
exclude=c(exclude,grep("TATA STEEL LTD-PARTLY PAID",out$longname))
if(length(exclude)>0){
        out=out[-exclude,]
}

# rownames(out)=out$exchangesymbol
# ibsymbols=select(out,ibsymbol)
# ibsymbols$ibsymbol=sapply(ibsymbols$ibsymbol,charToRaw)
# ibsymbols.list <- setNames(split(ibsymbols, seq(nrow(ibsymbols))), rownames(ibsymbols))
key=paste("ibsymbols",processingdate,sep=":")
r <- redux::hiredis()
r$SELECT(2)
for(i in seq_len(nrow(out))){
        r$HSET(key,out$exchangesymbol[i],(out$ibsymbol[i]))
}
r$QUIT()
print("Completed Universe update")


#### update nifty symbols ####
r <- redux::hiredis()
r$SELECT(2)
records=unlist(r$KEYS("nifty50*"))
lastrecord=sort(records)[length(records)]
list.old=unlist(r$SMEMBERS(lastrecord))
list.old=sort(list.old)

url="https://www1.nseindia.com/content/indices/ind_nifty50list.csv"
filename=paste("downloads/nifty50_",processingdate,".csv",sep="")
if(!file.exists(filename)){
        download.file(url, filename, method="libcurl", quiet = FALSE, mode = "w",cacheOK = TRUE,extra = "-L")
}
div <-  read.csv(filename, header = TRUE, stringsAsFactors = FALSE)
list.new=div$Symbol
list.new=sort(list.new)
match=sum(list.new==list.old)==(length(list.new)+length(list.old))/2
if(!match){
        key=paste("nifty50",processingdate,sep=":")
        for(s in list.new){
                r$SADD(key,(s))
        }
}
r$QUIT()
print("Completed Nifty50 update")

#### update CNX500 symbols ####
r <- redux::hiredis()
r$SELECT(2)
records=unlist(r$KEYS("cnx500*"))
lastrecord=sort(records)[length(records)]
list.old=unlist(r$SMEMBERS(lastrecord))
list.old=sort(list.old)

url="https://www1.nseindia.com/content/indices/ind_nifty500list.csv"
filename=paste("downloads/cnx500_",processingdate,".csv",sep="")
if(!file.exists(filename)){
        download.file(url, filename, method="libcurl", quiet = FALSE, mode = "w",cacheOK = TRUE,extra = "-L")
}
div <-  read.csv(filename, header = TRUE, stringsAsFactors = FALSE)
list.new=div$Symbol
list.new=sort(list.new)
match=sum(list.new==list.old)==(length(list.new)+length(list.old))/2
if(!match){
        key=paste("cnx500",processingdate,sep=":")
        for(s in list.new){
                r$SADD(key,(s))
        }
}
r$QUIT()
print("Completed CNX500 update")


#### update contract size ####
r <- redux::hiredis()
r$SELECT(2)

url="https://www1.nseindia.com/content/fo/fo_mktlots.csv"
filename=paste("downloads/fno_",processingdate,".csv",sep="")
if(!file.exists(filename)){
        download.file(url, filename, method="libcurl", quiet = FALSE, mode = "w",cacheOK = TRUE,extra = "-L")
}
div <-  read.csv(filename, header = TRUE, stringsAsFactors = FALSE)
list.new=div$SYMBOL
list.new=trimws(list.new)
size.near.expiry=div[,names(div)[3]]
size.far.expiry=div[,names(div)[4]]

# remove labels in the middle of the csv
id.to.remove=which(list.new=="Symbol")
list.new=list.new[-10]
list.new=gsub("^NIFTY$","NSENIFTY",list.new)
size.near.expiry=as.numeric(size.near.expiry[-10])
size.far.expiry=as.numeric(size.far.expiry[-10])
names(size.near.expiry)=list.new
names(size.far.expiry)=list.new
size.near.expiry=na.exclude(size.near.expiry)
size.far.expiry=na.exclude(size.far.expiry)
if(grepl("X",names(div)[3])){
        vector=unlist(strsplit(names(div)[3],"\\."))
        year=substr(vector[1],2,3)
        near.expiry=paste(vector[2],".",year,".01",sep="")
        vector=unlist(strsplit(names(div)[4],"\\."))
        year=substr(vector[1],2,3)
        far.expiry=paste(vector[2],".",year,".01",sep="")
        
}else{
        near.expiry=paste(proper(names(div)[3]),".01",sep="")
        far.expiry=paste(proper(names(div)[4]),".01",sep="")
}
near.expiry=strptime(near.expiry,format="%b.%y.%d",tz="Asia/Kolkata")
far.expiry=strptime(far.expiry,format="%b.%y.%d",tz="Asia/Kolkata")
near.expiry=RTrade::getExpiryDate(near.expiry)
far.expiry=RTrade::getExpiryDate(far.expiry)
near.expiry=strftime(strptime(near.expiry,format="%Y-%m-%d",tz="Asia/Kolkata"),"%Y%m%d")
far.expiry=strftime(strptime(far.expiry,format="%Y-%m-%d",tz="Asia/Kolkata"),"%Y%m%d")
near.key=paste("contractsize:",near.expiry,sep="")
far.key=paste("contractsize:",far.expiry,sep="")

for(s in 1:length(size.near.expiry)){
        r$HSET(near.key,as.character(names(size.near.expiry[s])),(as.character(size.near.expiry[s])))
        }

for(s in 1:length(size.far.expiry)){
        r$HSET(far.key,as.character(names(size.far.expiry[s])),(as.character(size.far.expiry[s])))
}
r$QUIT()
print("Completed Contract Size update")


#### update strike ####
# get near strikes
r <- redux::hiredis()
r$SELECT(2)
near.key=paste("strikedistance:",near.expiry,sep="")
far.key=paste("strikedistance:",far.expiry,sep="")
folderForNearStrikes=paste("/home/psharma/onedrive/rfiles/data/daily/opt/",near.expiry,"/",sep="")
for(s in 1:length(size.near.expiry)){
        symbol=names(size.near.expiry[s])
        if(symbol=="NSENIFTY"){
                r$HSET(near.key,as.character(names(size.near.expiry[s])),(as.character("100")))
        }else{
                files=list.files(path=folderForNearStrikes,pattern=paste("^",symbol,sep=""))
                if(length(files)>2){
                        strikes=sapply(strsplit(files,"_"),"[",5)
                        strikes=sapply(strsplit(strikes,"\\.rds"),"[",1)
                        strikes=as.numeric(strikes)
                        strikes=sort(strikes)
                        strikes=diff(strikes)
                        strikes=strikes[strikes>0]
                        if(length(strikes)>=2){
                                strikedistance=max(strikes)
                                r$HSET(near.key,
                                                  as.character(names(size.near.expiry[s])),
                                                  (as.character(strikedistance)))
                        }
                }
        }
        
}

folderForFarStrikes=paste("/home/psharma/onedrive/rfiles/data/daily/opt/",far.expiry,"/",sep="")
for(s in 1:length(size.far.expiry)){
        symbol=names(size.far.expiry[s])
        if(symbol=="NSENIFTY"){
                r$HSET(near.key,as.character(names(size.far.expiry[s])),(as.character("100")))
        }else{
                files=list.files(path=folderForFarStrikes,pattern=paste("^",symbol,sep=""))
                if(length(files)>2){
                        strikes=sapply(strsplit(files,"_"),"[",5)
                        strikes=sapply(strsplit(strikes,"\\.rds"),"[",1)
                        strikes=as.numeric(strikes)
                        strikes=sort(strikes)
                        strikes=diff(strikes)
                        strikes=strikes[strikes>0]
                        if(length(strikes)>=2){
                                strikedistance=max(strikes)
                                r$HSET(far.key,as.character(names(size.far.expiry[s])),(as.character(strikedistance)))
                        }
                }
        }
        
}

print("Completed Strikes update")

