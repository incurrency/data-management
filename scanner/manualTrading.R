print(Sys.time())
symbol = "BANKNIFTY_IND___"
a = QuickChart(symbol, days = 100, realtime = TRUE)
a$yclose = lag(a$close)
trend=Trend(a$date,a$high,a$low,a$close)
a$ll=ifelse(a$low<lag(a$low) & a$high<lag(a$high),1,0)
a$hh=ifelse(a$high>lag(a$high) & a$low>lag(a$low),1,0)


position=ifelse(a$hh==1 & a$ll==0,"Long",ifelse(a$ll==1 & a$hh==0,"Short",NA_character_))
position = ifelse(is.na(position),ifelse(trend$updownbar==TRUE,"Long","Short"),position)
a$dayRetracement=0.5*ifelse(position=="Short",lag(a$high)-a$low, ifelse(position=="Long",-a$high+lag(a$low),NA_real_))
a$dayTradePrice = ifelse(position=="Short",a$dayRetracement+a$low, ifelse(position=="Long",a$high+a$dayRetracement,NA_real_))
a$lasthourRetracement = ifelse(position=="Long",0.33 * (pmin(a$low, a$yclose)-a$high),ifelse(position=="Short",0.33 * (pmax(a$high, a$yclose)-a$low),NA_real_))
requiredLastHourRetracement = last(a$lasthourRetracement) 

df=data.frame(symbol=symbol,hh=last(a$hh),ll=last(a$ll),dayRequiredRetracement=last(a$dayRetracement),dayTradePrice=last(a$dayTradePrice),lastHourRequiredRetracement=requiredLastHourRetracement,last=last(a$close))

a = QuickChart(symbol,
               days = 4,
               realtime = TRUE,
               src = "persecond")
indextime = as.POSIXlt(a$date[nrow(a)])
indextime$min = 15
indextime$hour = 9
indextime$sec = 0
a = a[a$date >= indextime, ]
if(last(position)=="Long"){
        dayActualRetracement=a$close-max(a$high)
}else if(last(position)=="Short"){ 
        dayActualRetracement=a$close-min(a$low)
}

indextime$min = 25
indextime$hour = 14
indextime$sec = 0
a = a[a$date >= indextime, ]

if(nrow(a)>0){
        lastHourActualMove=a$close-first(a$close)
}else{
        lastHourActualMove=NA_real_
}


if(nrow(a)>0){
        lastHourTradePrice = last(ifelse(position=="Short",requiredLastHourRetracement+first(a$close), ifelse(position=="Long",first(a$close)+requiredLastHourRetracement,NA_real_)))
}else{
        lastHourTradePrice=NA_real_
}



df1=data.frame(dayActualRetracement=last(dayActualRetracement),lastHourActualMove=last(lastHourActualMove),lastHourTradePrice=lastHourTradePrice,position=last(position))
df=cbind(df,df1)
df=df[,c(1,2,3,4,8,5,6,9,10,7,11)]
out=df

symbol = "NSENIFTY_IND___"
a = QuickChart(symbol, days = 100, realtime = TRUE)
a$yclose = lag(a$close)
trend=Trend(a$date,a$high,a$low,a$close)
a$ll=ifelse(a$low<lag(a$low) & a$high<lag(a$high),1,0)
a$hh=ifelse(a$high>lag(a$high) & a$low>lag(a$low),1,0)

position=ifelse(a$hh==1 & a$ll==0,"Long",ifelse(a$ll==1 & a$hh==0,"Short",NA_character_))
position = ifelse(is.na(position),ifelse(trend$updownbar==TRUE,"Long","Short"),position)
a$dayRetracement=0.5*ifelse(position=="Short",lag(a$high)-a$low, ifelse(position=="Long",-a$high+lag(a$low),NA_real_))
a$dayTradePrice = ifelse(position=="Short",a$dayRetracement+a$low, ifelse(position=="Long",a$high+a$dayRetracement,NA_real_))
a$lasthourRetracement = ifelse(position=="Long",0.33 * (pmin(a$low, a$yclose)-a$high),ifelse(position=="Short",0.33 * (pmax(a$high, a$yclose)-a$low),NA_real_))
requiredLastHourRetracement = last(a$lasthourRetracement)

df=data.frame(symbol=symbol,hh=last(a$hh),ll=last(a$ll),dayRequiredRetracement=last(a$dayRetracement),dayTradePrice=last(a$dayTradePrice),lastHourRequiredRetracement=requiredLastHourRetracement,last=last(a$close))

a = QuickChart(symbol,
               days = 4,
               realtime = TRUE,
               src = "persecond")
indextime = as.POSIXlt(a$date[nrow(a)])
#indextime = as.POSIXlt(Sys.time())
indextime$min = 15
indextime$hour = 9
indextime$sec = 0
a = a[a$date >= indextime, ]
if(last(position)=="Long"){
        dayActualRetracement=a$close-max(a$high)
}else if(last(position)=="Short"){ 
        dayActualRetracement=a$close-min(a$low)
}

indextime$min = 25
indextime$hour = 14
indextime$sec = 0
a = a[a$date >= indextime, ]

if(nrow(a)>0){
        lastHourActualMove=a$close-first(a$close)
}else{
        lastHourActualMove=NA_real_
}


if(nrow(a)>0){
        lastHourTradePrice = last(ifelse(position=="Short",requiredLastHourRetracement+first(a$close), ifelse(position=="Long",first(a$close)+requiredLastHourRetracement,NA_real_)))
}else{
        lastHourTradePrice=NA_real_
}



df1=data.frame(dayActualRetracement=last(dayActualRetracement),lastHourActualMove=last(lastHourActualMove),lastHourTradePrice=lastHourTradePrice,position=last(position))
df=cbind(df,df1)
df=df[,c(1,2,3,4,8,5,6,9,10,7,11)]
out=rbind(out,df)
print(out)
a=QuickChart("NSENIFTY_IND___",realtime=TRUE,days=100)
a=QuickChart("BANKNIFTY_IND___",realtime=TRUE,days=100)
a=QuickChart("NSENIFTY_IND___",realtime=TRUE,src="persecond",days=1)
a = QuickChart("BANKNIFTY_IND___",realtime = TRUE,src = "persecond",days=1)


